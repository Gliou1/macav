//
//  classBDD.swift
//  maCaV
//
//  Created by Gilles Ciantar on 12/08/2020.
//

import Foundation
import CoreData

public class monContainer {
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "maCAV")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
}

