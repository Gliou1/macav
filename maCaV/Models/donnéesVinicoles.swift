//
//  donnéesVinicoles.swift
//  Cave
//
//  Created by Gilles Ciantar on 21/07/2020.
//
//
//  donnéesVinicoles.swift
//  CaveAVin_CoreData
//
//  Created by Gilles Ciantar on 04/09/2017.
//  Copyright © 2017 Gilles Ciantar. All rights reserved.
//
let listeRégionVinicole = [ "Alsace","Jura","Midi Pyrénées","Champagne","Poitou Charentes","Beaujolais","Bordeaux","Bourgogne","Centre","Rhône","Savoie","Cote du Rhône","Languedoc Roussillon","Loire","Provence","Corse","Sud Ouest" ].sorted()

let listeTerroirs_RegionsVinicoles = ["ListeTerroir_Alsace","ListeTerroir_Beaujolais","ListeTerroir_Bordeaux","ListeTerroir_Bourgogne","ListeTerroir_CoteduRhone","ListeTerroir_LanguedocRoussillon","ListeTerroir_Loire","ListeTerroir_Provence","ListeTerroir_Corse","ListeTerroir_SudOuest"].sorted()

let listeTerroir_Alsace = ["GEWURZTRAMINER",
                     "ALSACE PINOT OU KLEVNER",
                     "RIESLING",
                     "SYLVANER",
                     "TOKAY-PINOT GRIS",
                     "CREMANT D'ALSACE"].sorted()
                     
let listeTerroir_Beaujolais = ["COTE DE BROUILLY",
                     "BROUILLY",
                     "FLEURIE"].sorted()
                     
let listeTerroir_Bordeaux = ["BARSAC",
                     "BORDEAUX",
                     "COTES DE BOURG",
                     "COTES DE CASTILLON",
                     "COTES DE FRANCS",
                     "ENTRE-DEUX-MERS",
                     "FRONSAC",
                     "GRAVES",
                     "HAUT-MEDOC",
                     "LISTRAC-MEDOC",
                     "MARGAUX",
                     "MEDOC",
                     "MOULIS EN MEDOC",
                     "PAUILLAC",
                     "PESSAC LEOGNAN",
                     "POMEROL",
                     "PREMIERES COTES DE BORDEAUX",
                     "SAINT-EMILION",
                     "SAINT-ESTEPHE",
                     "SAINT-JULIEN",
                     "SAUTERNES"].sorted()
                     
let listeTerroir_Bourgogne = ["BOURGOGNE",
                     "BOURGOGNE ALIGOTE",
                     "CHABLIS",
                     "CHASSAGNE-MONTRACHET",
                     "CLOS DE VOUGEOT",
                     "CORTON",
                     "COTE DE BEAUNE",
                     "COTE DE NUITS-VILLAGES",
                     "GEVREY-CHAMBERTIN",
                     "GIVRY",
                     "MACON",
                     "MERCUREY",
                     "MEURSAULT",
                     "NUIT-SAINT-GEORGES",
                     "POMMARD",
                     "POUILLY-FUISSE",
                     "RULLY",
                     "VOLNAY",
                     "VOSNE-ROMANEE"].sorted()
                     
let listeTerroir_CoteduRhone = ["CHATEAU-GRILLET",
                     "CHATEAUNEUF-DU-PAPE",
                     "CONDRIEU",
                     "CORNAS",
                     "COTE ROTIE",
                     "COTES DU RHONE",
                     "COTES DU RHONE-VILLAGES",
                     "COTES DU VENTOUX",
                     "CROZES-HERMITAGE",
                     "GIGONDAS",
                     "HERMITAGE"].sorted()
                     
let listeTerroir_LanguedocRoussillon = ["CORBIERES",
                     "COSTIERES DE NIMES",
                     "FITOU",
                     "COTEAUX DU LANGUEDOC",
                     "MINERVOIS",
                     "COTES DU ROUSSILLON",
                     "SAINT-CHINIAN"].sorted()
                     
let listeTerroir_Loire = ["ANJOU",
                     "BOURGUEIL",
                     "CHINON",
                     "COTEAUX DU LAYON",
                     "MUSCADET",
                     "POUILLY-FUME",
                     "SANCERRE",
                     "SAUMUR",
                     "TOURAINE",
                     "VOUVRAY"].sorted()
                     
let listeTerroir_Provence = ["BANDOL",
                     "BELLET",
                     "COTES DE PROVENCE"].sorted()
                     
let listeTerroir_Corse = ["AJACCIO",
                     "PATRIMONIO"].sorted()
                     
let listeTerroir_SudOuest = ["BERGERAC",
                     "BUZET",
                     "CAHORS",
                     "GAILLAC",
                     "JURANCON",
                     "MADIRAN",
                     "MONBAZILLAC"].sorted()

let listeCouleur_ = ["Blanc","Rosé","Rouge"].sorted()












