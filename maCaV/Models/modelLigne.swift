//
//  modelLigne.swift
//  CaV
//
//  Created by Gilles Ciantar on 20/07/2020.
//

import Foundation


struct VinTest {
    var nbB:Int
    let nom:String
    let domaine:String
    let regionVinicole:String
    let couleur:String
    let anneeVin:Int
    let gardeMin:Int
    let gardeMax:Int
    let photoVin:String
}

let vinList:[VinTest] = [
    VinTest(nbB: 5, nom: "Fleur", domaine: "CiantarCiantarCiantarCiantarCiantarCiantar", regionVinicole: "Bourgogne", couleur: "Rouge", anneeVin: 2001, gardeMin: 2, gardeMax: 5, photoVin: "DSC_0845"),
    VinTest(nbB: 2, nom: "Pomme", domaine: "Gilles", regionVinicole: "Côte du Rhône", couleur: "Rosé", anneeVin: 2014, gardeMin: 2, gardeMax: 5, photoVin: "DSC_0845"),
    VinTest(nbB: 1, nom: "Abricot", domaine: "Manon", regionVinicole: "Bourgogne", couleur: "Rouge", anneeVin: 2012, gardeMin: 2, gardeMax: 15, photoVin: "DSC_0845"),
    VinTest(nbB: 6, nom: "Pêche", domaine: "Ciantar", regionVinicole: "Bordeaux", couleur: "Blanc", anneeVin: 2012, gardeMin: 2, gardeMax: 25, photoVin: "DSC_0845"),
    VinTest(nbB: 10, nom: "Poire", domaine: "Pierre", regionVinicole: "Bourgogne", couleur: "Rouge", anneeVin: 2019, gardeMin: 2, gardeMax: 55, photoVin: "DSC_0845"),
    VinTest(nbB: 8, nom: "Grenadine", domaine: "Benoit", regionVinicole: "Bourgogne", couleur: "Blanc", anneeVin: 1987, gardeMin: 1, gardeMax: 1, photoVin: "DSC_0845")
]

