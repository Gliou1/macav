//
//  ContentView.swift
//  maCaV
//
//  Created by Gilles Ciantar on 07/08/2020.
//

import SwiftUI

struct VuePrincipale: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    // 1.
    @FetchRequest(
      // 2.
      entity: Vin.entity(),
      // 3.
      sortDescriptors: [
        NSSortDescriptor(keyPath: \Vin.nom, ascending: true)
      ]
      //,predicate: NSPredicate(format: "genre contains 'Action'")
      // 4.
    ) var vins: FetchedResults<Vin>


    @AppStorage("nbBTotal") var nbBTotal = 0
    var displaynbBTotal: String {
        if nbBTotal > 1 {
            return ("\(nbBTotal) Bouteilles.")
        } else {
            return ("\(nbBTotal) Bouteille.")
        }
    }
    var body: some View {
        NavigationView{
            //Section(header: , content: <#T##() -> _#>)
            List{
                ForEach(vins, id: \.nom ) {
                    ligneVinVue(vin: $0)
                }
//                .onDelete(perform: deleteVin)
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text(displaynbBTotal)
//                    nbBTotalView(nbBT: nbBTotal)
                        .font(Font.custom("Charter-Bold", size: 30))
                        .foregroundColor(Color(red: 1.0, green: 0.25, blue: 0.85, opacity: 1))
                }
            }
            .navigationBarItems(
                leading:
                    HStack {
                        NavigationLink(destination: optionVue()){Text("Options")
                            .font(Font.custom("AppleSDGothicNeo-Bold", size: 18))
                            .frame(width: 70, height: 25, alignment: .center)
                            .foregroundColor(Color(red: 0.5, green: 0.5, blue: 0.5, opacity: 0.75))
                        }
                    },
                trailing:
                    HStack {
                        NavigationLink(destination: addVinVue()){Text("+")
                            .font(Font.custom("AppleSDGothicNeo-Bold", size: 40))
                            .frame(width: 25, height: 25, alignment: .center)
                            .foregroundColor(.green)
                        }
                    }
            )
        }
    }
    func deleteVin() {
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        VuePrincipale()
    }
}
