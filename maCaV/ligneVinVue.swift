//
//  ligneVinVue.swift
//  maCaV
//
//  Created by Gilles Ciantar on 07/08/2020.
//

import SwiftUI

struct ligneVinVue: View {
    let vin:Vin
    @State var isnbBViewPresented = false
//    @State var vin:VinTest
    var body: some View {
        HStack()
        {
            Button(action:
                    {
                        self.isnbBViewPresented = true
                    })
            {
                Text("\(String())")
                    .onAppear{calculNbTotalB(nbB: Int(vin.nb))}
            }
            .buttonStyle(BorderlessButtonStyle())
            .sheet(isPresented: $isnbBViewPresented, onDismiss: {
                saveNbB(nbB: Int(vin.nb))
            }, content: {  })  //nbBView(nbB: Int(vin.nb)
            .padding(.leading ,5)
            .frame(width: 30, height: 30, alignment: .center)
            Text("vin.nom")
                .frame(width: 100, height: 25, alignment: .leading)
            Text("vin.domaine")
                .frame(width: .none, height: 25, alignment: .leading)
            Spacer()
            Text("") //\(String(vin.anneeVin))")
                .frame(width: 50, height: 25, alignment: .leading)
            Text("A garder \(String(vin.gardeMin))")
            if (vin.gardeMax != 1) {
                Text("à \(String(vin.gardeMax)) ans")
            } else {
                Text(" an")
            }
            Button(action: {print("Bouton Photo \(vin)")}, label: {
                Image("vin.photoVin")
                    .resizable()
                    .frame(width: 24 , height: 24)
                    .cornerRadius(3.0)
            })
            .buttonStyle(BorderlessButtonStyle())
            .padding(.trailing ,1)
        }
        .font(Font.custom("customFonts/Notethis", size: 18))
        .foregroundColor(.black)
        .frame(width: .none, height: 25)
//        .background((definitionCouleurLigne(couleur: vin.couleur)))
        .cornerRadius(3.0)
    }
}

//struct ligneVinVue_Previews: PreviewProvider {
//    static var previews: some View {
//        ligneVinVue()
//    }
//}
