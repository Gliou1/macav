//
//  maCaVApp.swift
//  maCaV
//
//  Created by Gilles Ciantar on 07/08/2020.
//

import SwiftUI

@main
struct maCaVApp: App {
    var body: some Scene {
        WindowGroup {
            VuePrincipale()
        }
    }
}
