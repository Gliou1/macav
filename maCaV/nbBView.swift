//
//  nbBView.swift
//  maCaV
//
//  Created by Gilles Ciantar on 07/08/2020.
//

import SwiftUI

struct nbBView: View {
    @Binding var nbB:Int
    var body: some View {
        HStack {
            VStack {
                HStack {
                    HStack {
                        Text("\(nbB)")
                            .font(Font.custom("customFonts/Noteworthy_Lt-Bold", size: 50))
                        Image("DSC_0845")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 50, height: 50.0, alignment: .center)
                            .clipShape(Circle())
                    }
                }
                HStack {
                    if nbB > 0{
                    Button(action: {
                        nbB -= 1
                    }) {
                        Text("-")
                            .font(.largeTitle)
                            .fontWeight(.black)
                            .padding(.trailing, 50.0)
                            .frame(width: 100.0, height: 100.0)
                    }} else {
                        Text(" ")
                            .frame(width: 100.0, height: 100.0)
                    }
                    Button(action: {
                        nbB += 1
                    }) {
                        Text("+")
                            .font(.largeTitle)
                            .fontWeight(.black)
                            .padding(.leading, 50.0)
                            .frame(width: 100.0, height: 100.0)
                    }
                }
                .padding(.top, 40.0)
            }
        }
    }
}

struct nbBView_Previews: PreviewProvider {
    @State static var nbB = 5
    static var previews: some View {
        nbBView(nbB: $nbB)
    }
}
